import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/index';

import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService
{
    public url: string = environment.api;
    constructor(private http: HttpClient) { }

    public post(url: string, body?: Object) : Observable<any>
    {
        return this.http.post(this.url + url, body);
    }

    public put(url: string, body?: Object) : Observable<any>
    {
        return this.http.put(this.url + url, body);
    }

    public get(url: string, params?: HttpParams, options?: any) : Observable<any>
    {
        return this.http.get(this.url + url, { params: params, responseType: options && options["responseType"] ? options["responseType"] :  "json" });
    }

    public delete(url: string, body?: Object) : Observable<any>
    {
        return this.http.delete(this.url + url, body);
    }
}
