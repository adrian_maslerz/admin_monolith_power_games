import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/index';
import { SuccessInterface } from '../../interfaces/success.interface';
import { Pagination } from '../../interfaces/pagination.interface';
import { Product } from '../../interfaces/product.interface';
import { datePickerObjectToTimestamp } from '../../utilities/form.utils';

@Injectable()
export class ProductsDataService
{
    constructor(private apiService: ApiService) { }

    public getProducts(data: any) : Observable<Pagination<Product>>
    {
        return this.apiService.get("/products", data);
    }

    public getProduct(id: string) : Observable<Product>
    {
        return this.apiService.get("/products/" + id);
    }

    public addProduct(data: any, file: File) : Observable<SuccessInterface>
    {
        //preparing data
        if(data["premiere"])
            data["premiere"] = datePickerObjectToTimestamp(data["premiere"]);
        if(data["languages"])
            data["languages"] = JSON.stringify(data["languages"]);
        if(data["game_modes"])
            data["game_modes"] = JSON.stringify(data["game_modes"]);

        const form = new FormData();
        Object.keys(data).forEach(key => {
            if(key == "image")
                form.append(key, file);
            else
                form.append(key, data[key]);
        })

        return this.apiService.post("/products", form);
    }

    public updateProduct(id: string, data: any, file: File) : Observable<SuccessInterface>
    {
        //preparing data
        if(data["premiere"])
            data["premiere"] = datePickerObjectToTimestamp(data["premiere"]);
        if(data["languages"])
            data["languages"] = JSON.stringify(data["languages"]);
        if(data["game_modes"])
            data["game_modes"] = JSON.stringify(data["game_modes"]);

        const form = new FormData();
        Object.keys(data).forEach(key => {
            if(key == "image")
                form.append(key, file);
            else
                form.append(key, data[key]);
        })

        return this.apiService.put("/products/" + id, form);
    }


    public deleteProduct(id: string) : Observable<SuccessInterface>
    {
        return this.apiService.delete("/products/" + id);
    }

    public uploadProductPhoto(data: any) : Observable<{ status: boolean, originalName: string, generatedName: string, msg: string, imageUrl: string }>
    {
        const form = new FormData();
        form.append("photo", data.blob());

        return this.apiService.post("/products/photos", form);
    }
}
