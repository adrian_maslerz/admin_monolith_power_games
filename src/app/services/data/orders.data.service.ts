import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/index';
import { SuccessInterface } from '../../interfaces/success.interface';
import { Pagination } from '../../interfaces/pagination.interface';
import { Order } from '../../interfaces/order.interface';

@Injectable()
export class OrdersDataService
{
    constructor(private apiService: ApiService) { }

    public getOrders(data: any) : Observable<Pagination<Order>>
    {
        return this.apiService.get("/orders", data);
    }

    public getOrder(id: string) : Observable<Order>
    {
        return this.apiService.get("/orders/" + id);
    }

    public updateReviewState(orderId: string, itemId: string, data: any) : Observable<SuccessInterface>
    {
        return this.apiService.put("/orders/" + orderId + "/items/" + itemId, data);
    }
}
