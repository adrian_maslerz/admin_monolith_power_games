import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoriesDataService } from '../../../../../services/data/categories.data.service';
import { ToastrService } from 'ngx-toastr';
import { getMessagesConfig } from '../../../../../utilities/form.utils';
import { HttpErrorResponse } from '@angular/common/http';
import { Category } from '../../../../../interfaces/category.interface';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {

    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any[] = [];
    public id: string = "";
    public parent: Category;

    constructor(
        private router: Router,
        private categoriesDataService: CategoriesDataService,
        private toastr: ToastrService,
        private route: ActivatedRoute,
        private modalService: NgbModal
    ) {}

    ngOnInit(): void
    {
        this.id = this.route.snapshot.params["id"];

        //init form
        this.form = new FormGroup({
            title: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
            parent: new FormControl(null)
        });

        //messages config
        const fieldsConfig = {
            title: ['required', { key: "maxlength", value: 100}]
        };
        this.messages = getMessagesConfig(fieldsConfig);

        //populating data
        if(this.id)
            this.getData();
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        const method = this.id ? this.categoriesDataService.updateCategory(this.id, this.form.value) : this.categoriesDataService.addCategory(this.form.value);

        method
            .subscribe(() => {
                this.inProgress = false;
                this.toastr.success("Category saved successfully!");
                this.router.navigate(["/categories"])
            },
            (error: HttpErrorResponse) => {
                this.inProgress = false;
            });
    }

    private getData(): void
    {
        this.categoriesDataService
            .getCategory(this.id)
            .subscribe((category: Category) => {
                this.form.patchValue({
                    title: category.title,
                    parent: category.parent?._id
                });

                if(category.parent)
                    this.parent = category.parent;
            });
    }

    public onSelectCategoryModalOpen(modal): void
    {
        this.modalService
            .open(modal,{ size: 'lg' })
            .result
            .then(result => {}, reason => {});
    }

    public onSelectedCategory(category: Category | null): void
    {
        this.form.get("parent").patchValue(category ? category._id : null);
        this.parent = category;
    }
}
