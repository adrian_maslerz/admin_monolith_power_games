import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CategoriesDataService } from '../../../../../services/data/categories.data.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { debounceTime } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Category } from '../../../../../interfaces/category.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit, OnDestroy {

    @ViewChild(CategoriesListComponent, { static: true })

    public rows: Category[] = [];
    public total: number = 0;
    public page: number = 1;
    public parent: Category = null;
    private sort: string = "createdDESC";
    public readonly results: number = 10;

    private toDelete: string = null;
    public inProgress: boolean = false;
    public form: FormGroup

    public table: CategoriesListComponent;
    private subscription: Subscription;

    constructor(
        private categoriesDataService: CategoriesDataService,
        private toastr: ToastrService,
        private modalService: NgbModal
    ) { }

    ngOnInit(): void
    {
        this.form = new FormGroup({
            search: new FormControl("")
        });

        //on search
        this.subscription = this.form.get("search")
            .valueChanges
            .pipe(debounceTime(400))
            .subscribe(() => {
                this.page = 1;
                this.getData();
            });

        this.page = 1;
        this.getData();
    }

    ngOnDestroy(): void
    {
        if(this.subscription)
            this.subscription.unsubscribe();
    }

    public onPageChange(data): void
    {
        this.page = (data.offset + 1)
        this.getData();
    }

    public onSortChange(data): void
    {
        this.sort = data.column.prop.split(".")[0] + data.newValue.toUpperCase();
        this.page = 1;
        this.getData();
    }

    public onParentFilter(category: Category | null): void
    {
        this.parent = (category == null) || (this.parent && this.parent._id == category._id) ? null : category;
        this.page = 1;
        this.getData();
    }

    private getData(): void
    {
        this.categoriesDataService
            .getCategories({
                page: this.page,
                search: this.form.get("search").value,
                sort: this.sort,
                results: this.results,
                parent: this.parent ? this.parent._id : null
            })
            .subscribe(results => {
                this.total = results.total;
                this.rows = results.results;
            })
    }

    public onDeleteModalOpen(modal, id: string): void
    {
        this.toDelete = id;
        this.modalService
            .open(modal)
            .result
            .then(
                result => {
                    this.toDelete = null;
                },
                reason => {
                    this.toDelete = null;
                }
            );
    }

    public onDelete(modal): void
    {
        if(this.toDelete)
        {
            this.inProgress = true;
            this.categoriesDataService
                .deleteCategory(this.toDelete)
                .subscribe(() =>
                    {
                        this.inProgress = false;
                        this.toastr.success("Category deleted!");
                        modal.dismiss();

                        this.page = 1;
                        this.onPageChange({ offset: this.page - 1 });
                    },
                    (error: HttpErrorResponse) =>
                    {
                        this.inProgress = false;
                        modal.dismiss();
                    });
        }
        else
            modal.dismiss();
    }
}
