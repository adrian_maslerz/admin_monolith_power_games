import { Component, OnInit, ViewChild } from '@angular/core';
import { OrdersDataService } from '../../../../services/data/orders.data.service';
import { Order } from '../../../../interfaces/order.interface';

@Component({
    selector: 'app-dashboad',
    templateUrl: './dashboad.component.html',
    styleUrls: ['./dashboad.component.scss'],
    providers: [ OrdersDataService ]
})
export class DashboadComponent implements OnInit
{

    @ViewChild(DashboadComponent, { static: true })

    public rows: Order[] = [];
    public total: number = 0;
    public page: number = 1;
    public readonly results: number = 10;

    public table: DashboadComponent;

    constructor(
        private ordersDataService: OrdersDataService
    ) { }

    ngOnInit(): void
    {
        this.page = 1;
        this.getData();
    }

    public onPageChange(data): void
    {
        this.page = (data.offset + 1)
        this.getData();
    }

    private getData(): void
    {
        this.ordersDataService
            .getOrders({
                page: this.page,
                results: this.results
            })
            .subscribe(results => {
                this.total = results.total;
                this.rows = results.results;
            })
    }
}
