import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrdersDataService } from '../../../../../services/data/orders.data.service';
import { Order } from '../../../../../interfaces/order.interface';
import { OrderItem } from '../../../../../interfaces/order-item.interface';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-order-details',
    templateUrl: './order-details.component.html',
    styleUrls: ['./order-details.component.scss'],
})
export class OrderDetailsComponent implements OnInit
{
    public id: string = '';
    public order: Order;
    public inProgress: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private ordersDataService: OrdersDataService,
        private toastr: ToastrService
    ) { }

    ngOnInit(): void
    {
        this.id = this.route.snapshot.params[ 'id' ];
        this.getData();
    }

    private getData(): void
    {
        this.ordersDataService
            .getOrder(this.id)
            .subscribe((order: Order) => this.order = order);
    }

    public onReviewApprove(item: OrderItem): void
    {
        this.inProgress = true;
        this.ordersDataService
            .updateReviewState(this.order._id, item._id, { approved: item.review.approved ? 0 : 1 })
            .subscribe(() => {
                this.inProgress = false;
                this.toastr.success('Review state updated successfully!');
                this.getData();
            })
    }
}
