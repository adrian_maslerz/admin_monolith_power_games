import { Component, OnInit, ViewChild } from '@angular/core';
import { Order } from '../../../../../interfaces/order.interface';
import { OrdersDataService } from '../../../../../services/data/orders.data.service';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss']
})
export class OrdersListComponent implements OnInit {

    @ViewChild(OrdersListComponent, { static: true })

    public rows: Order[] = [];
    public total: number = 0;
    public page: number = 1;
    private sort: string = "createdDESC";
    public readonly results: number = 10;

    public table: OrdersListComponent;

    constructor(
        private ordersDataService: OrdersDataService
    ) { }

    ngOnInit(): void
    {
        this.page = 1;
        this.getData();
    }

    public onPageChange(data): void
    {
        this.page = (data.offset + 1)
        this.getData();
    }

    public onSortChange(data): void
    {
        this.sort = data.column.prop.split(".")[0] + data.newValue.toUpperCase();
        this.page = 1;
        this.getData();
    }

    private getData(): void
    {
        this.ordersDataService
            .getOrders({
                page: this.page,
                sort: this.sort,
                results: this.results
            })
            .subscribe(results => {
                this.total = results.total;
                this.rows = results.results;
            })
    }
}
