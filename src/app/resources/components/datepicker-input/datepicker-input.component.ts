import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { handleValidationErrorMessage, handleValidationStateClass } from '../../../utilities/form.utils';

@Component({
    selector: 'app-datepicker-input',
    templateUrl: './datepicker-input.component.html',
    styleUrls: ['./datepicker-input.component.css']
})
export class DatepickerInputComponent {

    @Input() form: FormGroup;
    @Input() name: string;
    @Input() displayName: string = this.name;
    @Input() messages: any[] = [];
    @Input() placeholder: string = "yyyy-mm-dd";

    public formUtils = { handleValidationStateClass, handleValidationErrorMessage };

    constructor() { }
}
