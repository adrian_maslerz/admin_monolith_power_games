import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { handleValidationErrorMessage } from '../../../utilities/form.utils';

@Component({
    selector: 'app-checkbox-input',
    templateUrl: './checkbox-input.component.html',
    styleUrls: ['./checkbox-input.component.css']
})
export class CheckboxInputComponent
{
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() displayName: string = this.name;
    @Input() messages: any[] = [];

    public formUtils = { handleValidationErrorMessage };

    constructor() { }

}
