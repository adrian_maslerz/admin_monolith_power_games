import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Category } from '../../../interfaces/category.interface';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CategoriesDataService } from '../../../services/data/categories.data.service';
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'app-categories-modal',
    templateUrl: './categories-modal.component.html',
    styleUrls: ['./categories-modal.component.scss'],
    providers: [ CategoriesDataService ]
})
export class CategoriesModalComponent implements OnInit
{
    @Output() selectedCategory: EventEmitter<Category> = new EventEmitter();
    @ViewChild(CategoriesModalComponent, { static: true })

    public rows: Category[] = [];
    public total: number = 0;
    public page: number = 1;
    public parent: Category = null;
    private sort: string = "createdDESC";
    public readonly results: number = 10;


    public inProgress: boolean = false;
    public form: FormGroup

    public table: CategoriesModalComponent;
    private subscription: Subscription;

    constructor(
        private categoriesDataService: CategoriesDataService
    ) { }

    ngOnInit(): void
    {
        this.form = new FormGroup({
            search: new FormControl("")
        });

        //on search
        this.subscription = this.form.get("search")
            .valueChanges
            .pipe(debounceTime(400))
            .subscribe(() => {
                this.page = 1;
                this.getData();
            });

        this.page = 1;
        this.getData();
    }

    ngOnDestroy(): void
    {
        if(this.subscription)
            this.subscription.unsubscribe();
    }

    public onPageChange(data): void
    {
        this.page = (data.offset + 1)
        this.getData();
    }

    public onSortChange(data): void
    {
        this.sort = data.column.prop.split(".")[0] + data.newValue.toUpperCase();
        this.page = 1;
        this.getData();
    }

    public onParentFilter(category: Category | null): void
    {
        this.parent = (category == null) || (this.parent && this.parent._id == category._id) ? null : category;
        this.page = 1;
        this.getData();
    }

    private getData(): void
    {
        this.categoriesDataService
            .getCategories({
                page: this.page,
                search: this.form.get("search").value,
                sort: this.sort,
                results: this.results,
                parent: this.parent ? this.parent._id : null
            })
            .subscribe(results => {
                this.total = results.total;
                this.rows = results.results;
            })
    }
}
