import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './resources/pages/auth/auth.component';
import { LoginComponent } from './resources/pages/auth/login/login.component';
import { ForgotPasswordComponent } from './resources/pages/auth/forgot-password/forgot-password.component';
import { ForgotPasswordChangeComponent } from './resources/pages/auth/forgot-password-change/forgot-password-change.component';
import { LoggedComponent } from './resources/pages/logged/logged.component';
import { DashboadComponent } from './resources/pages/logged/dashboad/dashboad.component';
import { TextInputComponent } from './resources/components/text-input/text-input.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HttpBasicInterceptor } from './interceptors/http.basic.interceptor';
import { HeaderComponent } from './resources/components/header/header.component';
import { FooterComponent } from './resources/components/footer/footer.component';
import { SidebarComponent } from './resources/components/sidebar/sidebar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileComponent } from './resources/pages/logged/admin/profile/profile.component';
import { EditProfileComponent } from './resources/pages/logged/admin/edit-profile/edit-profile.component';
import { ChangeEmailComponent } from './resources/pages/logged/admin/change-email/change-email.component';
import { ChangePasswordComponent } from './resources/pages/logged/admin/change-password/change-password.component';
import { AdminComponent } from './resources/pages/logged/admin/admin.component';
import { CategoriesComponent } from './resources/pages/logged/categories/categories.component';
import { EditCategoryComponent } from './resources/pages/logged/categories/edit-category/edit-category.component';
import { CategoriesListComponent } from './resources/pages/logged/categories/categories-list/categories-list.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectInputComponent } from './resources/components/select-input/select-input.component';
import { ProductsComponent } from './resources/pages/logged/products/products.component';
import { ProductsListComponent } from './resources/pages/logged/products/products-list/products-list.component';
import { EditProductComponent } from './resources/pages/logged/products/edit-product/edit-product.component';
import { OrdersComponent } from './resources/pages/logged/orders/orders.component';
import { OrdersListComponent } from './resources/pages/logged/orders/orders-list/orders-list.component';
import { FileInputComponent } from './resources/components/file-input/file-input.component';
import { TextareaInputComponent } from './resources/components/textarea-input/textarea-input.component';
import { DatepickerInputComponent } from './resources/components/datepicker-input/datepicker-input.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { CategoriesModalComponent } from './resources/components/categories-modal/categories-modal.component';
import { OrderDetailsComponent } from './resources/pages/logged/orders/order-details/order-details.component';

@NgModule({
    declarations: [
        AppComponent,
        AuthComponent,
        LoginComponent,
        ForgotPasswordComponent,
        ForgotPasswordChangeComponent,
        LoggedComponent,
        DashboadComponent,
        TextInputComponent,
        TextareaInputComponent,
        SelectInputComponent,
        HeaderComponent,
        FooterComponent,
        SidebarComponent,
        ProfileComponent,
        EditProfileComponent,
        ChangeEmailComponent,
        ChangePasswordComponent,
        AdminComponent,
        CategoriesComponent,
        EditCategoryComponent,
        CategoriesListComponent,
        ProductsComponent,
        ProductsListComponent,
        EditProductComponent,
        OrdersComponent,
        OrdersListComponent,
        FileInputComponent,
        DatepickerInputComponent,
        CategoriesModalComponent,
        OrderDetailsComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({ positionClass: 'toast-bottom-right' }),
        NgbModule,
        NgxDatatableModule,
        EditorModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpBasicInterceptor,
            multi: true
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule
{
}
