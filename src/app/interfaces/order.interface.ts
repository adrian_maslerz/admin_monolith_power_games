import { UserCopy } from './user-copy.interface';
import { OrderItem } from './order-item.interface';
import { Payment } from './payment.interface';

export interface Order
{
    _id: string;
    items: OrderItem[];
    user: string;
    payment: Payment;
    user_details: UserCopy;
    total: number;
    products_count: number;
    pieces_count: number;
    created: number;
}
