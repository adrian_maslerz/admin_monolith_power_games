import { Address } from './address.interface';
import { Customer } from './customer.interface';

export interface User
{
    _id: string;
    email: string;
    first_name: string;
    last_name: string;
    phone_number: string;
    roles: string[];
    permissions: string[];
    address?: Address;
    customer?: Customer;
}
