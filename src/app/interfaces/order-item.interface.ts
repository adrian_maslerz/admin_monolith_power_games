import { ProductCopy } from './product-copy.interface';
import { Review } from './review.interface';

export interface OrderItem
{
    sent: boolean;
    codes: string[];
    _id: string;
    product: ProductCopy;
    review?: Review;
}
