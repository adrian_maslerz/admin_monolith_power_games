import { Card } from './card.interface';

export interface Payment
{
    card: Card;
    currency: string;
    description: string;
    amount: string;
}
